# Información
Lenguaje utilizado: Ruby 2.4.4  
Reporte de procedimiento: Doc_Proyecto.docx  
  
# Ingreso de expresión regular y validación de cadenas
Archivo principal: expresion_regular.rb  
Estado actual: completo  
El programa recibe una cadena y valida si es una expresión regular  
Luego se ingresa una cadena y se valida si es derivada de dicha expresión regular  
  
# Tabla de transiciones
Archivo principal: transition_table.rb  
Estado actual: incompleto  
Transiciones solo reconocen operador por "."  
Falta anexo con archivo expresion_regular.rb