class Estado
	def initialize(id,reGex)
		@id = id
		@transiciones = {}
		nonletters = [')','*','|','+','(','.']
		reGex.split("").each do |letra|
			if !nonletters.include?(letra)
				@transiciones[letra] = nil
				nonletters.push(letra)
			end
		end
	end
end

class AFDs	
    def initialize
    end
	
	def self.expresionAnterior(reGex,pos)
		i = 1
		if reGex[pos-i] == ')'
			expresion = ')'
			while (reGex[pos-i] != '(')
				nExpresion,n=expresionAnterior(reGex,pos-i)
				expresion.prepend(nExpresion)
				i += n + 1
			end
			return expresion,i
		else 
			return reGex[pos-1],0
		end
	end
	
	def self.agregarPuntos(reGex)
		i = 0
		stopChars1 = [')','*','|','+']
		stopChars2 = ['(','|']
		ogLength = reGex.length()
		for j in (0..ogLength-1)
			if j != ogLength-1
				if (!stopChars1.include?(reGex[j+1+i]) && !stopChars2.include?(reGex[j+i]))
					reGex.insert(j+1+i,".")
					i+=1
				end
			end
		end
		return reGex
	end
	
	reGex = 'AB(CD)'
	puts 'Expresion regular: ' + reGex	
	reGex = agregarPuntos(reGex)
	puts reGex
	estados = Array.new
	operadores = ['*','.','+']
	stopChars = ['(',')']
	parCount = 0
	j = 1
	for i in (0..reGex.length()-1)
		if reGex[i] == '.'
			estado = Estado.new(j,reGex)
			transiciones = estado.instance_variable_get(("@#{'transiciones'}"))
			if !stopChars.include?(reGex[i+1])
				transiciones[reGex[i+1]] = j+1
			else
				while reGex[i] != '('
					parCount += 1
					i += 1
				end
				transiciones[reGex[i+1]] = j+1
			end
			puts j,transiciones
			estado.instance_variable_set(:@transiciones,transiciones)
			estados.push(estado)
			j += 1
		end
	end
	estados.each { |estado| 
	puts 'Estado ' + estado.instance_variable_get(("@#{'estado'}")) + '\nTransiciones: '
	estado.instance_variable_get(("@#{'transiciones'}")).each do |key, array|
		puts "#{key}-----"
		puts array
	end }
	#	puts estados[i].instance_variable_get("@#{'estado'}")
	#end
	#estado1.instance_variable_get("@#{'id'}")
end